package com.temnoter.stepcounter.ui

import android.content.Context
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.text.InputType
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.temnoter.stepcounter.R
import com.temnoter.stepcounter.conrollers.AdapterDays
import kotlinx.android.synthetic.main.stepcounter_layout.*
import java.text.SimpleDateFormat
import java.util.*


class StepCounterFragment : Fragment(), SensorEventListener {

    private var countStepLastDay: Int = 0
    private lateinit var currentDateAndTime: String
    private var mSensorManager: SensorManager? = null
    private var isActivityRunning = false


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.stepcounter_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sdf = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())

        currentDateAndTime = sdf.format(Date())
        val shared = activity!!.getSharedPreferences("DAYS", Context.MODE_PRIVATE)

        countStepLastDay = shared.getInt("countSteps", 0)

        val calendar: Calendar = Calendar.getInstance()
        when (calendar.get(Calendar.DAY_OF_WEEK)) {
            Calendar.SUNDAY -> {
                week_text.text = "Воскресенье"
            }
            Calendar.MONDAY -> {
                week_text.text = "Понедельник"
            }
            Calendar.TUESDAY -> {
                week_text.text = "Вторник"
            }
            Calendar.WEDNESDAY -> {
                week_text.text = "Среда"
            }
            Calendar.THURSDAY -> {
                week_text.text = "Четверг"
            }
            Calendar.FRIDAY -> {
                week_text.text = "Пятница"
            }
            Calendar.SATURDAY -> {
                week_text.text = "Субота"
            }
        }

        date_text.text = currentDateAndTime
        val packageManager: PackageManager = activity!!.packageManager
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_COUNTER) && packageManager.hasSystemFeature(
                PackageManager.FEATURE_SENSOR_STEP_DETECTOR
            )
        ) {
            mSensorManager = context!!.getSystemService(Context.SENSOR_SERVICE) as SensorManager?
        } else {
            Toast.makeText(
                context,
                "This phone does not supports required sensor",
                Toast.LENGTH_SHORT
            ).show()
        }

        edit_day.setOnClickListener {
            openDialog()
        }


        val days = listOf(0, 1, 2, 3, 4, 5, 6, 7)
        var results = days.map {
            Result(
                shared.getInt("day${it}u", 0),
                shared.getInt("day${it}d", 0),
                (((shared.getInt("day${it}u", 0) * 1.15 * 75 * 70) / 100000).toInt()),
                shared.getString("day${it}", "00.00.0000")!!
            )
        }.filter { it.day != "00.00.0000" }

        if (results.isNotEmpty() && currentDateAndTime != results.first().day) {
            val currentResult = Result(0, 0, 0, currentDateAndTime)
            shared.edit().putInt("day0u", currentResult.dayu).apply()
            shared.edit().putInt("day0d", currentResult.dayd).apply()
            shared.edit().putString("day0", currentResult.day).apply()
            for (i in 1 until results.size) {
                shared.edit().putInt("day${i}u", results[i - 1].dayu).apply()
                shared.edit().putInt("day${i}d", results[i - 1].dayd).apply()
                shared.edit().putString("day${i}", results[i - 1].day).apply()
            }

            results = days.map {
                Result(
                    shared.getInt("day${it}u", 0),
                    shared.getInt("day${it}d", 0),
                    (((shared.getInt("day${it}u", 0) * 1.15 * 75 * 70) / 100000).toInt()),
                    shared.getString("day${it}", "00.00.0000")!!
                )
            }.filter { it.day != "00.00.0000" }
        }
        if (results.isNotEmpty()) {
            count_step_planned_text.text = "/${results.first().dayd}"
            count_step_text.text = results.first().dayu.toString()
            count_kkal_text.text =
                (((results.first().dayu * 1.15 * 75 * 70) / 100000).toInt()).toString()
        }
        Log.i("TEST_REVIEW", results.toString())
        last_week.adapter =
            AdapterDays(context!!, results)
    }

    data class Result(val dayu: Int, val dayd: Int, val kkal: Int, val day: String)

    private fun openDialog() {
        val builder =
            MaterialAlertDialogBuilder(
                ContextThemeWrapper(
                    context!!,
                    R.style.CustomAlertDialogTheme
                )
            )
        val input = EditText(context!!)
        input.inputType = InputType.TYPE_CLASS_NUMBER
        val maxLength = 7
        val fArray = mutableListOf<InputFilter>()
        fArray.add(LengthFilter(maxLength))
        input.filters = fArray.toTypedArray()
        builder.setView(input).setPositiveButton(
            "OK"
        ) { dialog, which ->
            count_step_planned_text.text = "/${("0" + input.text.toString()).toInt()}"
            val shared = activity!!.getSharedPreferences("DAYS", Context.MODE_PRIVATE)
            shared.edit().putInt("day0d", ("0" + input.text.toString()).toInt()).apply()

        }
            .setTitle("Какое кол-во шагов хотите преодолеть за сегодня?")
            .show()
    }

    override fun onResume() {
        super.onResume()
        isActivityRunning = true
        val countSensor =
            mSensorManager!!.getDefaultSensor(Sensor.TYPE_STEP_COUNTER)
        val detectSensor =
            mSensorManager!!.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR)
        if (countSensor != null) {
            mSensorManager!!.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_UI)
        } else {
            Toast.makeText(context, "Count sensor not available!", Toast.LENGTH_LONG).show()
        }
        if (detectSensor != null) {
            mSensorManager!!.registerListener(this, detectSensor, SensorManager.SENSOR_DELAY_UI)
        } else {
            Toast.makeText(context, "Count sensor not available!", Toast.LENGTH_LONG).show()
        }
    }

    override fun onPause() {
        super.onPause()
        isActivityRunning = false
    }

    override fun onAccuracyChanged(sensor: Sensor?, p1: Int) {}

    override fun onSensorChanged(sensorEvent: SensorEvent?) {
        when (sensorEvent!!.sensor.type) {
            Sensor.TYPE_STEP_COUNTER -> if (isActivityRunning) {
                count_step_text.text = sensorEvent.values[0].toInt().toString()
                count_kkal_text.text =
                    (((sensorEvent.values[0] * 1.15 * 75 * 70) / 100000).toInt()).toString()

                val shared = activity!!.getSharedPreferences("DAYS", Context.MODE_PRIVATE)

                if (shared.getString("day0", "").isNullOrEmpty() || shared.getString(
                        "day0",
                        ""
                    ) != currentDateAndTime
                ) {
                    shared.edit().putInt("countSteps", sensorEvent.values[0].toInt()).apply()
                }
                shared.edit().putInt("day0u", sensorEvent.values[0].toInt()).apply()
                shared.edit().putString("day0", currentDateAndTime).apply()
            }
            Sensor.TYPE_STEP_DETECTOR -> {
            }
        }
    }
}