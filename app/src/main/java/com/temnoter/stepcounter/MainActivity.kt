package com.temnoter.stepcounter

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.temnoter.stepcounter.ui.StepCounterFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        supportFragmentManager.beginTransaction()
            .replace(R.id.main_container,
                StepCounterFragment()
            ).commitNow()
    }
}
