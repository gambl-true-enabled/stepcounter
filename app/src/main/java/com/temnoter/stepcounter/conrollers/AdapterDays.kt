package com.temnoter.stepcounter.conrollers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.temnoter.stepcounter.R
import com.temnoter.stepcounter.ui.StepCounterFragment
import kotlinx.android.synthetic.main.stepcounter_layout.*
import java.text.SimpleDateFormat
import java.util.*


class AdapterDays(
    private val context: Context,
    private val results: List<StepCounterFragment.Result>
) :
    RecyclerView.Adapter<AdapterDays.DayViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayViewHolder {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.previous_day_card, parent, false)
        return DayViewHolder(view)
    }

    override fun getItemCount() = results.size-1

    override fun onBindViewHolder(holder: DayViewHolder, position: Int) {
        holder.onBind(position)
    }


    inner class DayViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val date = view.findViewById<TextView>(R.id.date_text_card)
        private val week = view.findViewById<TextView>(R.id.week_text_card)
        private val kkalText = view.findViewById<TextView>(R.id.kkal_text_card)
        private val upCount = view.findViewById<TextView>(R.id.up_count_card)
        private val downCount = view.findViewById<TextView>(R.id.down_count_card)
        fun onBind(position: Int) {
            val sdf = SimpleDateFormat("dd.MM.yyyy")
            sdf.parse(results[position + 1].day)
            val calendar = Calendar.getInstance(sdf.timeZone)

            when (calendar.get(Calendar.DAY_OF_WEEK)) {
                Calendar.SUNDAY -> {
                    week.text = "Воскресенье"
                }
                Calendar.MONDAY -> {
                    week.text = "Понедельник"
                }
                Calendar.TUESDAY -> {
                    week.text = "Вторник"
                }
                Calendar.WEDNESDAY -> {
                    week.text = "Среда"
                }
                Calendar.THURSDAY -> {
                    week.text = "Четверг"
                }
                Calendar.FRIDAY -> {
                    week.text = "Пятница"
                }
                Calendar.SATURDAY -> {
                    week.text = "Субота"
                }

            }

            date.text = results[position + 1].day
            kkalText.text =
                (((results[position + 1].dayu * 1.15 * 75 * 70) / 100000).toInt()).toString()
            upCount.text = results[position + 1].dayu.toString()
            downCount.text = results[position + 1].dayd.toString()
        }
    }
}